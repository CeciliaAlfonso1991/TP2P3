package TP2.progra3.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import TP2.progra3.datos.PuntoDeCarbon;
import TP2.progra3.mapeo.Aplicacion;
import TP2.progra3.mapeo.Mapa;

class AlgoritmoTest {
	
	@Test
	Aplicacion testAlgoritmo() 
	{
		Mapa mapa = new Mapa();
	
		PuntoDeCarbon p1 = new PuntoDeCarbon("1",20);
		PuntoDeCarbon p2 = new PuntoDeCarbon("2",0);
		PuntoDeCarbon p3 = new PuntoDeCarbon("3",20);
		PuntoDeCarbon p4 = new PuntoDeCarbon("4",26);
		PuntoDeCarbon p5 = new PuntoDeCarbon("5",6);
		PuntoDeCarbon p6 = new PuntoDeCarbon("6",35);
		
		mapa.agregarPuntoDeCarbon(p1);
		mapa.agregarPuntoDeCarbon(p2);
		mapa.agregarPuntoDeCarbon(p3);
		mapa.agregarPuntoDeCarbon(p4);
		mapa.agregarPuntoDeCarbon(p5);
		mapa.agregarPuntoDeCarbon(p6);
		
		mapa.agregarTunel(p1, p2,2, "a1");
		mapa.agregarTunel(p1, p3,8, "a2");
		mapa.agregarTunel(p2, p3,5, "a3");
		mapa.agregarTunel(p2, p4,3, "a4");
		mapa.agregarTunel(p3, p2,6, "a5");
		mapa.agregarTunel(p3, p5,0, "a6");
		mapa.agregarTunel(p4, p3,1, "a7");
		mapa.agregarTunel(p4, p5,7, "a8");
		mapa.agregarTunel(p5, p4,4, "a9");
		mapa.agregarTunel(p4, p6,6, "a10");
		mapa.agregarTunel(p6, p5,2, "a11");
		
		
	
		Aplicacion app = new Aplicacion(mapa);
		return app;
	}
	
	@Test
	void testMejorCamino() 
	{
		Aplicacion app = testAlgoritmo();
		app.ingresarCarbonMinimoRequerido(20);
		app.setOrigen(app.getMapaMina().getPuntos().getFirst());
		assertEquals(5, app.mejorCamino());	
	}
	@Test
	void testMejorCamino1() 
	{
		Aplicacion app = testAlgoritmo();
		app.ingresarCarbonMinimoRequerido(20);
		app.setOrigen(app.getMapaMina().getPuntos().get(1));
		assertEquals(3, app.mejorCamino());	
	}
	@Test
	void testMejorCamino2() 
	{
		Aplicacion app = testAlgoritmo();
		app.ingresarCarbonMinimoRequerido(30);
		app.setOrigen(app.getMapaMina().getPuntos().get(2));
		assertEquals(10, app.mejorCamino());	
	}
	@Test
	void testMejorCamino3() 
	{
		Aplicacion app = testAlgoritmo();
		app.ingresarCarbonMinimoRequerido(20);
		app.setOrigen(app.getMapaMina().getPuntos().getLast());
		assertEquals(6, app.mejorCamino());	
	}
}
