package TP2.progra3.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import TP2.progra3.mapeo.Aplicacion;
import TP2.progra3.mapeo.Mapa;

class AplicacionTest 
{
	@Test
	Aplicacion testAplicacion() {
		Mapa mapa = new Mapa();
		Aplicacion app = new Aplicacion(mapa);
		return app;
	}

	@Test(excepted = IllegalArgumentException.class)
	void testIngresarCarbonMinimoRequerido()
	{
		Aplicacion app = testAplicacion();
		app.ingresarCarbonMinimoRequerido(-50);
	}

}
