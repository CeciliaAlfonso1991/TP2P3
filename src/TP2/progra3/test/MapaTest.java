package TP2.progra3.test;

import static org.junit.jupiter.api.Assertions.*;
import TP2.progra3.datos.PuntoDeCarbon;
import TP2.progra3.mapeo.Mapa;
import org.junit.jupiter.api.Test;

class MapaTest {

	@Test
	Mapa testMapa() {
		Mapa mapa = new Mapa();
		return mapa;
	}

	@Test
	void testCompareTo() {
		fail("Not yet implemented");
	}

	@Test(excepted = IllegalArgumentException.class)
	void testAgregarTunel() 
	{
		Mapa mapa = testMapa();
		PuntoDeCarbon p1 = new PuntoDeCarbon("1",20);
		PuntoDeCarbon p2 = new PuntoDeCarbon("2",10);
		mapa.agregarPuntoDeCarbon(p1);
		mapa.agregarPuntoDeCarbon(p2);
		mapa.agregarTunel(p1, p2,2, "a1");
		mapa.agregarTunel(p1, p2,2, "a1");

	}

	@Test(excepted = IllegalArgumentException.class)
	void testAgregarPuntoDeCarbonStringInt() {
		Mapa mapa = new Mapa();
		mapa.agregarPuntoDeCarbon("",-36);	
	}

	@Test(excepted = IllegalArgumentException.class)
	void testAgregarPuntoDeCarbonPuntoDeCarbon() {
		Mapa mapa = new Mapa();
		PuntoDeCarbon p1 = new PuntoDeCarbon("",-20);
		mapa.agregarPuntoDeCarbon(p1);	
	}

}
