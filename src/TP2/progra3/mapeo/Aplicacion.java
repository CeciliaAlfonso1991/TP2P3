package TP2.progra3.mapeo;

import TP2.progra3.datos.*;

public class Aplicacion 
{
	private Mapa mapaMina;
	private int carbonMinimoRequerido;
	private Algoritmo algoritmo;
	private int puntosDeCarbon, tiempoMaximo;
	private PuntoDeCarbon origen;
	
	public Aplicacion(Mapa mapa) 
	{
		inicializarVariables(mapa);
	}

	private void inicializarVariables(Mapa mapa)
	{
		mapaMina = mapa;
		tiempoMaximo = 10000000;
		algoritmo = new Algoritmo(mapa, tiempoMaximo);
		puntosDeCarbon = mapaMina.getCantidadPuntosDeCarbon();
		carbonMinimoRequerido = 20;
		origen = new PuntoDeCarbon("",0);
	}
	
	public int mejorCamino() 
	{	
		return algoritmo.algoritmo(origen, carbonMinimoRequerido);
	}

	public int getCarbonMinimoRequerido()
	{
		return carbonMinimoRequerido;
	}
	
	public void ingresarCarbonMinimoRequerido(int cantidad) 
	{
		if(cantidad<0) 
		{
			throw new IllegalArgumentException("El minimo requerido de carbon debe ser mayor que cero");
		}
		carbonMinimoRequerido = cantidad;
	}
	
	public Mapa getMapaMina() 
	{
		return mapaMina;
	}

	public void setMapaMina(Mapa mapaMina) 
	{
		this.mapaMina = mapaMina;
	}

	public int getPuntosDeCarbon() {
		return puntosDeCarbon;
	}

	public void setPuntosDeCarbon(int puntosDeCarbon) 
	{
		this.puntosDeCarbon = puntosDeCarbon;
	}

	public PuntoDeCarbon getOrigen() 
	{
		return origen;
	}

	public void setOrigen(PuntoDeCarbon origen) 
	{
		verificarOrigen(origen);
		this.origen = origen;
	}

	private void verificarOrigen(PuntoDeCarbon origen) 
	{
		if(origen == null) {
			throw new IllegalArgumentException("el origen debe ser distinto de null");
		}
		if(!mapaMina.getPuntos().contains(origen)) {
			throw new IllegalArgumentException("el origen debe ser igual a algun punto regitrado en el mapa");
		}
	}
	
	public int getTiempoMaximo() 
	{
		return tiempoMaximo;
	}

	public void setTiempoMaximo(int tiempoMaximo)
	{
		verificarTiempoMaximo(tiempoMaximo);
		this.tiempoMaximo = tiempoMaximo;
	}

	private void verificarTiempoMaximo(int tiempoMaximo) 
	{
		if(tiempoMaximo<0) {
			throw new IllegalArgumentException("El algoritmo no admite tiempos negativos");
		}
	}
}
