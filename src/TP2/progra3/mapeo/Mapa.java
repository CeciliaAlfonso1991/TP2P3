package TP2.progra3.mapeo;

import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeSet;
import TP2.progra3.datos.Tunel;
import TP2.progra3.datos.PuntoDeCarbon;

public class Mapa 
{
	private SortedSet<Tunel<PuntoDeCarbon,PuntoDeCarbon>> tuneles;
	private	LinkedList<PuntoDeCarbon> puntosDeCarbon;
	
	public Mapa() 
	{
		inicializarVariables(); 
	}

	private void inicializarVariables()
	{
		puntosDeCarbon = new LinkedList<PuntoDeCarbon>();
		tuneles =new TreeSet<Tunel<PuntoDeCarbon,PuntoDeCarbon>>();
	}
	
	public LinkedList<PuntoDeCarbon> getPuntos()
	{
		return puntosDeCarbon;
	}
	public int getCantidadPuntosDeCarbon()
	{
		return puntosDeCarbon.size();
	}
	
	public SortedSet<Tunel<PuntoDeCarbon,PuntoDeCarbon>> getTuneles() 
	{
		return tuneles;
	}

	public void agregarTunel(PuntoDeCarbon p1, PuntoDeCarbon p2, Integer tiempo, String nombre) 
	{
		verificarTunel(p1, p2, tiempo, nombre);
		Tunel<PuntoDeCarbon,PuntoDeCarbon> tunel = new Tunel<PuntoDeCarbon,PuntoDeCarbon>(p1,p2, tiempo, nombre);
		verificarExistenciaDeTunel(tunel);
		tuneles.add(tunel);
	}
	
	private void verificarExistenciaDeTunel(Tunel<PuntoDeCarbon,PuntoDeCarbon> tunel)
	{
		for(Tunel<PuntoDeCarbon,PuntoDeCarbon> t :tuneles)
		{
			if(t.getEntrada().equals(tunel.getEntrada())&&t.getSalida().equals(tunel.getSalida())&& t.getTiempo()==tunel.getTiempo()&&t.getNombre().equals(tunel.getNombre())) {
				throw new IllegalArgumentException("Tunel existente");
			}
		}
	}

	private void verificarTunel(PuntoDeCarbon p1, PuntoDeCarbon p2, Integer tiempo, String nombre) 
	{
		if(p1 == null || p2 == null) {
			throw new IllegalArgumentException("Ambos puntos deben ser distinto de null");
		}
		if(tiempo.compareTo(0)<0) {
			throw new IllegalArgumentException("No puede tener tiempo negativo");
		}
		if(nombre.equals("")) {
			throw new IllegalArgumentException("Colocar un nombre NO vacio");
		}
	}

	public void agregarPuntoDeCarbon(String string, int cantidadDeCarbon)
	{
	
		verificarPuntoDeCarbon(string, cantidadDeCarbon);		
		PuntoDeCarbon p = new PuntoDeCarbon(string, cantidadDeCarbon);
		verificarExistenciaDePuntoDeCarbon(p);
		puntosDeCarbon.add(p);
	}

	public void agregarPuntoDeCarbon(PuntoDeCarbon p) 
	{	
		verificarExistenciaDePuntoDeCarbon(p);
		puntosDeCarbon.add(p);
	}
	
	private void verificarExistenciaDePuntoDeCarbon(PuntoDeCarbon p)
	{
		for(PuntoDeCarbon pto : puntosDeCarbon) {
			if(pto.getNombre().equals(p.getNombre())) {
				throw new IllegalArgumentException("Punto de interes existente") ;
			}
		}
	}

	private void verificarPuntoDeCarbon(String string, int cantidadDeCarbon)
	{
		if(string.equals(""))
		{
			throw new IllegalArgumentException("El nombre del punto de carbon no puede ser vacio");
		}
		if(cantidadDeCarbon<0) 
		{
			throw new IllegalArgumentException("la cantidad de carbon del punto de interes no puede ser negativa");
		}
	}
}
