package TP2.progra3.mapeo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeMap;
import TP2.progra3.datos.Tunel;
import TP2.progra3.datos.PuntoDeCarbon;

public class Algoritmo 
	{
	private SortedSet<Tunel<PuntoDeCarbon,PuntoDeCarbon>> tuneles;
	private LinkedList<PuntoDeCarbon> vecinosVisitados;
	private TreeMap<Integer, PuntoDeCarbon> distanciasTentativasVecinosCercanos;
	private TreeMap<PuntoDeCarbon,Integer> distanciaTentativaPorPuntoActualizada;
	private HashMap<PuntoDeCarbon,LinkedList<PuntoDeCarbon>> recorridosPorPunto;
	private LinkedList<PuntoDeCarbon> puntosDeCarbon;
	private PuntoDeCarbon puntoActual;
	private int tiempoMaximo;

	public Algoritmo(Mapa mapa, int tiempoMaximo2)
	{
		inicializarVariables(mapa, tiempoMaximo2);
	}

	private void inicializarVariables(Mapa mapa, int tiempoMaximo2) 
	{
		tuneles = mapa.getTuneles(); 
		puntosDeCarbon= mapa.getPuntos();
		vecinosVisitados = new LinkedList<PuntoDeCarbon>();
		distanciasTentativasVecinosCercanos = new TreeMap<Integer, PuntoDeCarbon>();
		distanciaTentativaPorPuntoActualizada = new TreeMap<PuntoDeCarbon, Integer>();
		recorridosPorPunto = new HashMap<PuntoDeCarbon,LinkedList<PuntoDeCarbon>>();
		tiempoMaximo = tiempoMaximo2;
		puntoActual = new PuntoDeCarbon("",0);
	}

	public int algoritmo(PuntoDeCarbon origen, Integer cantidad) 
	{
		puntoActual = origen;
		LinkedList<PuntoDeCarbon> vecinosNoVisitados = puntosDeCarbon;
		asignarDistanciasTentativas(puntoActual);
		while(!vecinosNoVisitados.isEmpty() && !vecinosVisitados.contains(puntoActual))
		{		
				vecinosVisitados.add(puntoActual);
				vecinosNoVisitados.remove(puntoActual);	
				actualizoDistanciaTentativa(puntoActual);
				actualizoPuntoActualPorPuntoNoVisitadoMenor();
		}
		agregoPuntosActualizadosFaltantes(vecinosNoVisitados);
		return puntoTiempoMinimoConCarbon(origen, cantidad) ;
	}

	private void asignarDistanciasTentativas(PuntoDeCarbon origen) 
	{
		puntosDeCarbon.forEach((k) -> 
		{
			if(k.equals(origen)) {
				distanciaTentativaPorPuntoActualizada.put(k,0);
			}
			else {
				distanciaTentativaPorPuntoActualizada.put(k, tiempoMaximo);		
			}
		});
	}
	
	private void actualizoDistanciaTentativa(PuntoDeCarbon puntoActual) 
	{	
		for(Tunel<PuntoDeCarbon,PuntoDeCarbon> tunel : tuneles)
		{	
			if(tunel.esEntrada(puntoActual)) 
			{	
				evaluoDistancia(tunel);					
			}
		}
	}

	private void evaluoDistancia(Tunel<PuntoDeCarbon, PuntoDeCarbon> tunel)
	{
		int distancia = distanciaTentativaPorPuntoActualizada.get(tunel.getEntrada())+ tunel.getTiempo();
		if(distancia < distanciaTentativaPorPuntoActualizada.get(tunel.getSalida()))
		{	
			agregarPuntosRecorridosDe(tunel.getSalida());
			distanciasTentativasVecinosCercanos.put(distancia,tunel.getSalida());
			distanciaTentativaPorPuntoActualizada.replace(tunel.getSalida(), distancia);
		}
	}

	private void actualizoPuntoActualPorPuntoNoVisitadoMenor()
	{
		if(!distanciasTentativasVecinosCercanos.isEmpty()) 
		{
			this.puntoActual = distanciasTentativasVecinosCercanos.firstEntry().getValue();
		}
		distanciasTentativasVecinosCercanos = new TreeMap<Integer, PuntoDeCarbon>();
	}
	
	private boolean agregoPuntosActualizadosFaltantes(LinkedList<PuntoDeCarbon> vecinosNoVisitados) {
		return vecinosVisitados.addAll(vecinosNoVisitados);
	}

	private void agregarPuntosRecorridosDe(PuntoDeCarbon ptoActual)
	{	
		LinkedList <PuntoDeCarbon> recorridoHasta = new LinkedList<PuntoDeCarbon>();
		recorridoHasta.addAll(vecinosVisitados);
		recorridosPorPunto.put(ptoActual,recorridoHasta);
	}

	private Integer puntoTiempoMinimoConCarbon(PuntoDeCarbon origen, Integer cantidad)
	{
		for(PuntoDeCarbon pto : vecinosVisitados) 
		{
				if(!pto.equals(origen) && pto.getCantidadDeCarbon()>= cantidad) {
					puntoActual = pto;
					break;
				}
		}
		return distanciaTentativaPorPuntoActualizada.get(puntoActual);
	}
}
	

