package TP2.progra3.datos;


public class PuntoDeCarbon implements Comparable<PuntoDeCarbon>
{	
	private String nombre;
	private int cantidadDeCarbon;
	
	public PuntoDeCarbon(String nombre, int cantidadDeCarbon) 
	{
		inicializarVariables(nombre, cantidadDeCarbon);
	}

	private void inicializarVariables(String nombre, int cantidadDeCarbon) 
	{
		this.nombre = nombre;
		this.cantidadDeCarbon = cantidadDeCarbon;
	}
	
	public int getCantidadDeCarbon() 
	{
		return cantidadDeCarbon;
	}
	
	public void setCantidadDeCarbon(int cantidadDeCarbon)
	{
		if(cantidadDeCarbon<0) 
		{
			throw new IllegalArgumentException("Debe ser mayor que 0");
		}
		this.cantidadDeCarbon = cantidadDeCarbon;
	}
	
	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public boolean tieneCarbon() 
	{
		return cantidadDeCarbon >= 0  ? true : false ;
	}

	public boolean equals(PuntoDeCarbon p) {
       
		if(nombre.equals(p.getNombre())) {
			return true;
		}
		return false;
    }

	@Override
	public boolean equals(Object obj) {
		PuntoDeCarbon other = (PuntoDeCarbon) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	@Override
	public int compareTo(PuntoDeCarbon arg0) {
		// TODO Auto-generated method stub
		if(arg0.nombre.charAt(0)<nombre.charAt(0)) {
			return 1;
		}
		if(arg0.nombre.charAt(0)>nombre.charAt(0)) {
			return -1;
		}
		return 0;
	}
	@Override
	public String toString() {
		return "PuntoDeCarbon [nombre=" + nombre + "]";
	}
}
