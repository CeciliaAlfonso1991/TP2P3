package TP2.progra3.datos;

public class Tunel<T1, T2> implements Comparable<T1>{

	private T1 entrada;
	private T2 salida;
	private int tiempo;
	private String nombre;
	
	public Tunel(T1 t1, T2 t2, Integer tiempo2, String nombre2)
	{
		inicializarVariables(t1, t2, tiempo2, nombre2);
	}

	private void inicializarVariables(T1 t1, T2 t2, Integer tiempo2, String nombre2) 
	{
		entrada = t1;
		salida = t2;
		tiempo = tiempo2;
		nombre = nombre2;
	}
	
	public boolean esEntrada(T1 t1) {
		if(entrada.equals(t1)) {
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entrada == null) ? 0 : entrada.hashCode());
		result = prime * result + ((salida == null) ? 0 : salida.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
	
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Tunel<T1, T2> other = (Tunel<T1, T2>) obj;
		if (entrada == null) {
			if (other.entrada != null)
				return false;
		} else if (!entrada.equals(other.entrada))
			return false;
		if (salida == null) {
			if (other.salida != null)
				return false;
		} else if (!salida.equals(other.salida))
			return false;
		return true;
	}


	@Override
	public int compareTo(Object arg0) 
	{
		@SuppressWarnings("unchecked")
		Tunel<T1,T2> other = (Tunel<T1,T2>) arg0; 
		if(other.getTiempo()<=tiempo) {
			return 1;
		}
		if(other.getTiempo()>tiempo) {
			return -1;
		}
		return 0;
	}
	public T1 getEntrada() {
		return entrada;
	}

	public void setEntrada(T1 elemento1) {
		this.entrada = elemento1;
	}

	public T2 getSalida() {
		return salida;
	}

	public void setSalida(T2 elemento2) {
		this.salida = elemento2;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "Tunel [Entrada=" + entrada.toString() + ", Salida=" + salida.toString() + ", tiempo=" + tiempo + ", Nombre="
				+ nombre + "]";
	}

}
